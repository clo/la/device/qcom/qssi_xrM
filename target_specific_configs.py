#Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
#SPDX-License-Identifier: BSD-3-Clause-Clear

target_deprecate_kernel_headers = ["qssi_xrM", "niobe"]
target_deprecate_target_out_headers = ["qssi_xrM", "niobe"]
